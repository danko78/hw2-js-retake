let number = +prompt("Enter the number");
while (isNaN(number) || !Number.isInteger(number) || number === 0) {
    number = +prompt("Enter the number");
}

if (number < 5) {
    console.log("Sorry no numbers");
} else {
    for (i = 0; i <= number; i++) {
        if (i !== 0 && i % 5 === 0) {
            console.log(i);
        }
    }
}